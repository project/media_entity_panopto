<?php

namespace Drupal\media_entity_panopto;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Component\Render\MarkupTrait;

class PanoptoMarkup implements MarkupInterface {

  use MarkupTrait;

}
